# DCC Level 0 Character Generator

Python Project
This program generates a userspecified number of Level 0 peasants for Dungeon Crawl Classsics.
The goal is to have the generated characters be output to a file to be printed.

When the program is started, the user is greeted by a small window. It contains general instructions, an entry label, and a button to activate the generator.
When a integer is specified and "GENERATE" is clicked, the program will create the specified number of level 0 peasants.
Upon Generation, a small dialogue box will appear confirming the generation. The peasants are output to a file saved in the same directory as the program.