#DCC level 0 character generator
import random
import sys
from tkinter import *

def generator():
	
	conmessage = "Generation completed, check filesystem."
	orig_stdout = sys.stdout
	wfile = open('Generatoroutput.txt', 'w')
	sys.stdout = wfile
	
	fnamep1 = ['adg', 'bog', 'skul', 'wog', 'wilk', 'wik', 'aik', 'gil', 'ker', 'ch', 'sab', 'pant', 'jud', 'scar', 'ska', 'ze', 'wig']
	
	fnamep2 = ['wig', 'gs', 'tls', 'wog', 'stab', 'ilk', 'son', 'erman', 'erson', 'worth', 'ger', 'sdf', 'as', 'aton', 'era', 'na', 'ris']
	
	abilmods = [-3, -2, -2, -1, -1, -1, 0, 0, 0, 0, 1, 1, 1, 2, 2, 3]
	
	joblist = ["1 Alchemist Staff Oil 1 flask",
	"2 Animal trainer Club Pony",
	"3 Armorer Hammer (as club) Iron helmet",
	"4 Armorer Hammer (as club) Iron helmet",
	"5 Astrologer Dagger Spyglass",
	"6 Blacksmith Hammer (as club) Steel tongs",
	"7 Blacksmith Hammer (as club) Steel tongs",
	"8 Blacksmith Hammer (as club) Steel tongs",
	"9 Caravan guard [Short sword] Linen, 1 yard",
	"10 Caravan guard [Short sword] Linen 1 yard",
	"11 Cobbler Awl (as dagger) Shoehorn",
	"12 Confidence artist Dagger Quality cloak",
	"13 Cooper Crowbar (as club) Barrel",
	"14 Cutpurse Dagger Small chest",
	"15 Cutpurse Dagger Small chest",
	"16 Ditch digger Shovel (as staff) Fine dirt, 1 lb.",
	"17 Ditch digger Shovel (as staff) Fine dirt, 1 lb.",
	"18 Dwarven blacksmith Hammer (as club) Mithril, 1 oz.",
	"19 Dwarven blacksmith Hammer (as club) Mithril, 1 oz.",
	"20 Dwarven blacksmith Hammer (as club) Mithril, 1 oz.",
	"21 Dwarven blacksmith Hammer (as club) Mithril, 1 oz.",
	"22 Dwarven herder Staff Sow**",
	"23 Dwarven herder Staff Sow**",
	"24 Dwarven miner Pick (as club) Lantern",
	"25 Dwarven miner Pick (as club) Lantern",
	"26 Dwarven miner Pick (as club) Lantern",
	"27 Dwarven miner Pick (as club) Lantern",
	"28 Elven artisan Staff Clay, 1 lb.",
	"29 Elven artisan Staff Clay, 1 lb.",
	"39 Elven artisan Staff Clay, 1 lb.",
	"31 Elven artisan Staff Clay, 1 lb.",
	"32 Elven forester Staff Herbs, 1 lb.",
	"33 Elven forester Staff Herbs, 1 lb.",
	"34 Elven forester Staff Herbs, 1 lb.",
	"35 Elven forester Staff Herbs, 1 lb.",
	"36 Elven sage Dagger Parchment and quill pen",
	"37 Elven sage Dagger Parchment and quill pen",
	"38 Farmer* Pitchfork (as spear) Hen**",
	"39 Farmer* Pitchfork (as spear) Rooster**",
	"40 Farmer* Pitchfork (as spear) Goose**",
	"41 Farmer* Pitchfork (as spear) Sheep**",
	"42 Farmer* Pitchfork (as spear) Ram**",
	"43 Farmer* Pitchfork (as spear) Goat**",
	"44 Farmer* Pitchfork (as spear) Donkey**",
	"45 Farmer* Pitchfork (as spear) Hen**",
	"46 Farmer* Pitchfork (as spear) Hen**",
	"47 Farmer* Pitchfork (as spear) Hen**",
	"48 Fortune-teller Dagger Tarot deck",
	"49 Gambler Club Dice",
	"50 Gongfarmer Trowel (as dagger) Sack of night soil",
	"51 Grave digger Shovel (as staff) Trowel",
	"52 Grave digger Shovel (as staff) Trowel",
	"53 Guild beggar Sling Crutches",
	"54 Guild beggar Sling Crutches",
	"55 Halfling gypsy Sling Hex doll",
	"56 Halfling gypsy Sling Hex doll",
	"57 Halfling gypsy Sling Hex doll",
	"58 Halfling gypsy Sling Hex doll",
	"59 Halfling trader short sword 20 sp",
	"60 Halfling trader short sword 20 sp",
	"61 Halfling trader short sword 20 sp",
	"62 Halfling trader short sword 20 sp",
	"63 Halfling vagrant Club Begging bowl",
	"64 Halfling vagrant Club Begging bowl",
	"65 Healer Club Holy water, 1 vial",
	"66 Herbalist Club Herbs, 1 lb.",
	"67 Herder Staff Herding dog**",
	"68 Herder Staff Herding dog**",
	"69 Herder Staff Herding dog**",
	"70 Hunter Shortbow Deer pelt",
	"71 Hunter Shortbow Deer pelt",
	"72 Hunter Shortbow Deer pelt",
	"73 Indentured servant Staff Locket",
	"74 Jester Dart Silk clothes",
	"75 Jeweler Dagger Gem worth 20 gp",
	"76 Locksmith Dagger Fine tools",
	"77 Mercenary Longsword Hide armor",
	"78 Miller/baker Club Flour, 1 lb.",
	"79 Minstrel Dagger Ukulele",
	"80 Noble Longsword Gold ring worth 10 gp",
	"81 Orphan Club Rag doll",
	"82 Ostler Staff Bridle",
	"83 Outlaw Short sword Leather armor",
	"84 Scribe Dart Parchment, 10 sheets",
	"85 Shaman Mace Herbs, 1 lb.",
	"86 Slave Club Strange-looking rock",
	"87 Smuggler Sling Waterproof sack",
	"88 Soldier Spear Shield",
	"89 Soldier Spear Shield",
	"90 Squire Longsword Steel helmet",
	"91 Squire Longsword Steel helmet",
	"92 Trapper Sling Badger pelt",
	"93 Trapper Sling Badger pelt",
	"94 Urchin Stick (as club) Begging bowl",
	"95 Wainwright Club Pushcart***",
	"96 Weaver Dagger Fine suit of clothes",
	"97 Wizard apprentice Dagger Black grimoire",
	"98 Woodcutter Handaxe Bundle of wood",
	"99 Woodcutter Handaxe Bundle of wood",
	"100 Woodcutter Handaxe Bundle of wood"]
	
	charnumpoint = 0
	try:
	    charnumvalue = int(e1.get())
	except ValueError:
	    print("Please enter an integer! Example: 1, 12, or 100")
	    return;
	stre = 0
	strmod = 0
	agi = 0
	agimod = 0
	sta = 0
	stamod = 0
	per = 0
	permod = 0
	intel = 0
	intmod = 0
	luc = 0
	lucmod = 0
	
	while charnumpoint < charnumvalue:
	    charnumpoint += 1
	    stre = random.randint(3, 18)
	    agi = random.randint(3, 18)
	    sta = random.randint(3, 18)
	    per = random.randint(3, 18)
	    intel = random.randint(3, 18)
	    luc = random.randint(3, 18)
	    strmod = abilmods[stre - 3]
	    agimod = abilmods[agi - 3]
	    stamod = abilmods[sta - 3]
	    permod = abilmods[per - 3]
	    intmod = abilmods[intel - 3]
	    lucmod = abilmods[luc - 3]
	    #fortsv = stamod
	    #reflsv = agimod
	    #willsv = permod
	    hpval = random.randint(1,4) + stamod
	    if hpval <= 0:
	        hpval = 1
	    acval = 10
	    lucsign = random.randint(1, 30)
	    job = random.randint(0, 99)
	    
	    print(random.choice(fnamep1) + random.choice(fnamep2)) 
	    print("Level: 0      Class: Peasant")
	    print("STR " + str(stre) + " " + str(strmod) + " " + "AGI " + str(agi) + " " + str(agimod) + " " + "STA " + str(sta) + " " + str(stamod) + " " + "PER " + str(per) + " " + str(permod) + " " + "INT " + str(intel) + " " + str(intmod) + " " + "LUC " + str(luc) + " " + str(lucmod) )
	    print("Fortitude: +0 Willpower: +0 Reflex: +0")
	    print("Armor: " + str(acval))
	    print("Health: " + str(hpval))
	    print("Job, Weapon, Trade Good: " + joblist[job])
	    print("")
	print("Done. Have fun storming the castle! ")
	sys.stdout = orig_stdout
	wfile.close()
	conf = Tk()
	conf.title("Done")
	topframe = Frame(conf)
	topframe.pack()
	cmsg = Message(topframe, text = conmessage, width = 250)
	cmsg.pack()
	qbutton = Button(topframe, text = "Okay", command = conf.destroy)
	qbutton.pack(side = BOTTOM)
	conf.mainloop
	return

instructmessage = "First type the number of peasents into the box, then click generate!"

root = Tk()
root.title("PEASENT GENERATOR!")
topframe = Frame(root)
topframe.pack()
bottomframe = Frame(root)
bottomframe.pack(side = BOTTOM)
instmsg = Message(topframe, text = instructmessage, width = 250)
instmsg.pack(side = LEFT)
e1 = Entry(topframe, width = 10)
e1.pack(side = RIGHT)
button = Button(bottomframe, text = "GENERATE!", command = generator)
button.pack(side = BOTTOM)
print("How many unfortunate souls do you wish to prepare? ")
#chargennumber = int(input())
print("")
#generator(chargennumber)

root.mainloop
